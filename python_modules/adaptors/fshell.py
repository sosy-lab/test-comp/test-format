# This file is part of the Test-Comp test format,
# an exchange format for test suites:
# https://gitlab.com/sosy-lab/software/test-format
#
# SPDX-FileCopyrightText: 2018-2019 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

import os

from adaptors import utils
from adaptors.testcase_converter import TestConverter


class Preprocessor:
    def prepare_file(self, filename, error_method=None):
        if error_method:
            excludes = [error_method]
        else:
            excludes = ()
        with open(filename, "r", encoding="UTF-8") as inp:
            filecontent = inp.read()
        nondet_methods = utils.find_nondet_methods(filecontent, excludes=excludes)
        return self.prepare(filecontent, nondet_methods, error_method)

    def prepare(self, filecontent, nondet_methods_used, error_method=None):
        content = filecontent
        content += "\n"
        content += utils.EXTERNAL_DECLARATIONS
        content += "\n"
        content += utils.get_assume_method()
        content += "\n"
        if error_method:
            content += utils.get_error_method_definition(error_method)
        for method in nondet_methods_used:
            # append method definition at end of file content
            nondet_method_definition = self._get_nondet_method_definition(
                method["name"], method["type"], method["params"]
            )
            content += nondet_method_definition

        # FShell ignores __VERIFIER_nondet methods. We rename them so that they are
        # analyzed correctly
        content = content.replace("__VERIFIER_nondet", "___VERIFIER_nondet")

        return content

    @staticmethod
    def _get_nondet_method_definition(method_name, method_type, param_types):
        var_name = utils.get_sym_var_name(method_name)
        method_head = utils.get_method_head(method_name, method_type, param_types)
        method_body = ["{"]
        if method_type != "void":
            if "float" in method_type or "double" in method_type:
                conversion_cmd = "strtold({0}, 0);".format(var_name)
            elif "unsigned" in method_type and "long long" in method_type:
                conversion_cmd = "strtoull({0}, 0, 10);".format(var_name)
            else:
                conversion_cmd = "strtoll({0}, 0, 10);".format(var_name)
            return_statement = "return ({0}) {1}".format(method_type, conversion_cmd)
            method_body += [
                "char * {0} = malloc(1000);".format(var_name),
                "fgets({0}, 1000, stdin);".format(var_name),
                return_statement,
            ]
        method_body = "\n    ".join(method_body)
        method_body += "\n}\n"

        return method_head + method_body

    @staticmethod
    def _get_error_method_definition(error_method):
        return (
            "void "
            + error_method
            + '() {{ fprintf(stderr, "{0}\\n"); }}\n'.format(utils.ERROR_STRING)
        )


class FshellTestConverter(TestConverter):
    def get_test_cases_in_dir(self, directory=None, exclude=()):
        if directory is None:
            directory = "."
        tests_file = os.path.join(directory, "testsuite.txt")
        if os.path.exists(tests_file):
            with open(tests_file, "r", encoding="UTF-8") as inp:
                content = [l.strip() for l in inp.readlines()]
            if len([l for l in content if "Test Suite" in l]) > 1:
                raise AssertionError("More than one test suite exists in " + tests_file)

            curr_test = []
            test_cases = []
            count = 0
            for line in content:
                if line.startswith("IN:"):
                    test_name = str(count)
                    if test_name not in exclude and count > 0:
                        test_cases.append(
                            utils.TestCase(test_name, tests_file, curr_test)
                        )
                    curr_test = []
                    count += 1
                if line.startswith("strto"):
                    test_value = line.split("=")[1]
                    curr_test.append(test_value)
            test_name = str(count)
            if curr_test and test_name not in exclude:
                test_cases.append(utils.TestCase(test_name, tests_file, curr_test))
            return test_cases
        return []

    def get_test_case_from_file(self, test_file):
        """
        Not supported. It is not possible to create a single test case.

        see _get_test_cases_in_dir instead.

        :raises NotImplementedError: when called
        """
        raise NotImplementedError(
            "FShell can only create test cases for the full test suite"
        )

    def get_test_vector(self, test_case):
        vector = utils.TestVector(test_case.name, test_case.origin)
        for t in test_case.content:
            vector.add(t)

        return vector

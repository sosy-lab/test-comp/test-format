# This file is part of the Test-Comp test format,
# an exchange format for test suites:
# https://gitlab.com/sosy-lab/software/test-format
#
# SPDX-FileCopyrightText: 2018-2019 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

import os

import adaptors.afl
import adaptors.cpatiger
import adaptors.crest
import adaptors.fshell
import adaptors.klee

TEST_DIR = os.path.join(os.path.dirname(__file__), "../test/")
TEST_FILE = os.path.join(TEST_DIR, "simple.c")


def test_preprocessor():
    for adaptor in (
        adaptors.afl,
        adaptors.crest,
        adaptors.cpatiger,
        adaptors.fshell,
        adaptors.klee,
    ):
        assert _does_preprocessing_produce_some_output(
            adaptor
        ), "Prepared content is empty"


def _does_preprocessing_produce_some_output(module):
    preprocessor = module.Preprocessor()

    prepared_content = preprocessor.prepare_file(TEST_FILE)

    return prepared_content


def test_convert_afl():
    directory = os.path.join(TEST_DIR, "afl/")
    converter = adaptors.afl.AflTestConverter()

    vectors = converter.get_test_vectors(directory)

    assert len(vectors) == 5, "Expected 5, size: %s" % len(vectors)


def test_convert_cpatiger():
    directory = os.path.join(TEST_DIR, "cpatiger/")
    converter = adaptors.cpatiger.CpaTigerTestConverter()

    vectors = converter.get_test_vectors(directory)

    assert len(vectors) == 2, "Expected 2, size: %s" % len(vectors)


def test_convert_crest():
    directory = os.path.join(TEST_DIR, "crest/")
    converter = adaptors.crest.CrestTestConverter()

    vectors = converter.get_test_vectors(directory)

    assert len(vectors) == 1, "Expected 1, size: %s" % len(vectors)


def test_convert_fshell():
    directory = os.path.join(TEST_DIR, "fshell/")
    converter = adaptors.fshell.FshellTestConverter()

    vectors = converter.get_test_vectors(directory)

    assert len(vectors) == 2, "Expected 2, size: %s" % len(vectors)


def test_convert_klee():
    directory = os.path.join(TEST_DIR, "klee/")
    converter = adaptors.klee.KleeTestConverter()

    vectors = converter.get_test_vectors(directory)

    assert len(vectors) == 3, "Expected 3, size: %s" % len(vectors)

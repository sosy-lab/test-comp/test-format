# This file is part of the Test-Comp test format,
# an exchange format for test suites:
# https://gitlab.com/sosy-lab/software/test-format
#
# SPDX-FileCopyrightText: 2018-2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

import glob
import logging
import os

from adaptors import utils
from adaptors.testcase_converter import TestConverter


class Preprocessor:
    def prepare_file(self, filename, error_method=None):
        if error_method:
            excludes = [error_method]
        else:
            excludes = ()
        with open(filename, "r", encoding="UTF-8") as inp:
            filecontent = inp.read()
        nondet_methods = utils.find_nondet_methods(filecontent, excludes=excludes)
        return self.prepare(filecontent, nondet_methods, error_method)

    def prepare(self, filecontent, nondet_methods_used, error_method=None):
        content = filecontent
        content += "\n"
        content += "#include<crest.h>\n"
        content += "\n"
        content += utils.EXTERNAL_DECLARATIONS
        content += "\n"
        content += utils.get_assume_method()
        content += "\n"
        if error_method:
            content += utils.get_error_method_definition(error_method)
        for method in nondet_methods_used:
            # append method definition at end of file content
            nondet_method_definition = self._get_nondet_method_definition(
                method["name"], method["type"], method["params"]
            )
            content += nondet_method_definition
        return content

    def _get_nondet_method_definition(self, method_name, method_type, param_types):
        if not (method_type == "void" or self.is_supported_type(method_type)):
            logging.warning(
                "Crest can't handle symbolic values of type %s", method_type
            )
            internal_type = "unsigned long long"
            logging.warning(
                "Continuing with type %s for method %s", internal_type, method_name
            )
        elif method_type == "_Bool":
            internal_type = "char"
        else:
            internal_type = method_type
        var_name = utils.get_sym_var_name(method_name)
        marker_method_call = "CREST_" + "_".join(internal_type.split())
        method_head = utils.get_method_head(method_name, method_type, param_types)
        method_body = ["{"]
        if method_type != "void":
            method_body += [
                "{0} {1};".format(internal_type, var_name),
                "{0}({1});".format(marker_method_call, var_name),
            ]

            if method_type == internal_type:
                method_body.append("return {0};".format(var_name))
            else:
                method_body.append("return ({0}) {1};".format(method_type, var_name))

        method_body = "\n    ".join(method_body)
        method_body += "\n}\n"

        return method_head + method_body

    @staticmethod
    def is_supported_type(method_type):
        return method_type in [
            "_Bool",
            "long long",
            "long long int",
            "long",
            "long int",
            "int",
            "short",
            "char",
            "unsigned long long",
            "unsigned long long int",
            "unsigned long",
            "unsigned long int",
            "unsigned int",
            "unsigned short",
            "unsigned char",
        ]


class CrestTestConverter(TestConverter):
    @staticmethod
    def _get_file_name(filename):
        return os.path.basename(filename)

    def get_test_cases_in_dir(self, directory=None, exclude=()):
        if directory is None:
            directory = "."
        tcs = []
        for t in glob.glob(directory + "/input*"):
            test_name = self._get_file_name(t)
            if test_name not in exclude and not test_name.endswith("license"):
                tcs.append(self.get_test_case_from_file(t))
        return tcs

    def get_test_case_from_file(self, test_file):
        with open(test_file, "r", encoding="UTF-8") as inp:
            content = inp.read()
        return utils.TestCase(self._get_file_name(test_file), test_file, content)

    def get_test_vector(self, test_case):
        test_vector = utils.TestVector(test_case.name, test_case.origin)
        for line in test_case.content.split("\n"):
            value = line.strip()
            if value:
                test_vector.add(value)
        return test_vector
